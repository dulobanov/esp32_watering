import network
import ntptime
from machine import WDT, reset, Pin
from time import sleep

from libs.log import LOGGER
from settings import WATERING_PIN


class _State:
    def __init__(self):
        self._wdt = WDT(timeout=30000)
        self._wlan = None
        self._watering_pin = Pin(WATERING_PIN, Pin.OUT)

    def reset_watering(self):
        self._watering_pin.off()

    def alive(self):
        self._wdt.feed()

    def connect_to_wifi(self, essid, password):
        LOGGER.write("Connecting to {} ...".format(essid), send_to_server=False, write_to_file=True)
        self._wlan = network.WLAN(network.STA_IF)
        self._wlan.active(True)
        self._wlan.config(reconnects=3)
        self._wlan.connect(essid, password)

        sec = 0
        while not self._wlan.isconnected():
            if sec > 20:
                reset()
            LOGGER.write(
                "Connecting to {}, sec {} ...".format(essid, sec),
                send_to_server=False,
                write_to_file=True,
            )
            sec += 1
            self.alive()
            sleep(1)
        if self._wlan.isconnected():
            sleep(1)
            ntptime.settime()
            LOGGER.write(
                "Ifconfig", self._wlan.ifconfig(), self._wlan.status("rssi"),
                send_to_server=False,
                write_to_file=True,
            )

        else:
            LOGGER.write("Unable connect to WiFi {}".format(essid), send_to_server=False, write_to_file=True)

        self.alive()
        return self._wlan


STATE = _State()
