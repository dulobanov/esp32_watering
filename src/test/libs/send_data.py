import ujson
from time import time
from libs.telemetry_sender import TelemetrySample, TelemetrySender


def test_measurement():
    sender = TelemetrySender(
        url="https://192.168.2.95:8443/hash-api/measurements/",
        fingerprint="63fe109e-2265-4914-aa9a-826258d62255",
        hash_salt="957fed38-8b51-4939-885a-8f92067956d6",
    )
    return sender.send_measurements((
        TelemetrySample(
            timestamp=time(),
            measurement="measurement",
            value=time(),
        ),
    ))


def test_log():
    sender = TelemetrySender(
        url="https://192.168.2.95:8443/hash-api/board-logs/",
        fingerprint="63fe109e-2265-4914-aa9a-826258d62255",
        hash_salt="957fed38-8b51-4939-885a-8f92067956d6",
    )
    return sender.send_data(
        ujson.dumps({"message": "Test message {}".format(time())}),
    )
