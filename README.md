#Setup environment
virtualenv -p python3 .venv
source .venv/bin/activate
pip install -r src/requirements.txt

## Upload micropython
esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20190125-v1.10.bin
