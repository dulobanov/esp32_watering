import ujson
from collections import namedtuple

from libs.data_sender import DataSender
from libs.dt import iso_time_string_from_timestamp
from settings import MEASUREMENTS_SERVER_URL, BOARD_FINGERPRINT, BOARD_HASH_SALT

TelemetrySample = namedtuple("TelemetrySample", ("timestamp", "measurement", "value"))


class _TelemetrySender(DataSender):
    def send_measurements(self, samples):
        send_data = []
        for sample in samples:
            if isinstance(sample, TelemetrySample):
                send_data.append({
                    "datetime": iso_time_string_from_timestamp(sample.timestamp),
                    "measurement": sample.measurement,
                    "value": sample.value,
                })

        return self.send_data(data=ujson.dumps(send_data))


telemetry_sender = _TelemetrySender(
    url=MEASUREMENTS_SERVER_URL,
    fingerprint=BOARD_FINGERPRINT,
    hash_salt=BOARD_HASH_SALT,
)
