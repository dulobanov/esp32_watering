import gc
from hashlib import sha256
from machine import reset
from ubinascii import hexlify
from urequests import post


class DataSender:
    def __init__(self, url, fingerprint, hash_salt):
        self._url = url
        self._fingerprint = fingerprint
        self._hash_salt = hash_salt

        self._headers = {
            "Content-Type": "application/json",
            "Station": fingerprint,
        }

    def send_data(self, data: str):
        if all((self._url, self._fingerprint, self._hash_salt)):
            gc.collect()

            headers = self._headers.copy()
            headers["Signature"] = hexlify(sha256((data + self._hash_salt).encode()).digest())

            try:
                response = post(
                    self._url,
                    headers=headers,
                    data=data,
                )
            except Exception as exc:
                print("Can not send data, ", exc)
                reset()

            gc.collect()
        else:
            print("Sending to server is skipped, not all fields are provided")
        return response
