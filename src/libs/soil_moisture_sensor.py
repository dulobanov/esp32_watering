from machine import ADC, Pin


class SoilMoistureSensor:
    DRY_MOISTURE = 0
    AQUA_MOISTURE = 100

    def __init__(self, pin, dry_value, aqua_value):
        self._adc = ADC(Pin(pin), atten=ADC.ATTN_11DB)
        self._dry_value = dry_value
        self._aqua_value = aqua_value
        self._init()

    def __get__(self, instance, owner):
        return self.get_moisture()

    def _init(self):
        # https://premierdevelopment.ru/uravnenie-prjamoj.html
        # y = k * x + b

        # X1, Y1 - dry
        # X2, Y2 - aqua

        # A = Y2 - Y1
        a = self.AQUA_MOISTURE - self.DRY_MOISTURE

        # B = X1 - X2
        b = self._dry_value - self._aqua_value

        # C = X2 * Y1 - X1 * Y2
        c = self._aqua_value * self.DRY_MOISTURE - self._dry_value * self.AQUA_MOISTURE

        # k = -A / B
        self._k = -a / b

        # b = -C / B
        self._b = -c / b

    def get_raw_value(self):
        return self._adc.read_u16()

    def get_moisture(self):
        value = self._k * self.get_raw_value() + self._b
        if value < 0:
            value = 0
        elif value > 100:
            value = 100
        return value
