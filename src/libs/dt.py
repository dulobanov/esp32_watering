from time import time, localtime
from collections import namedtuple

from settings import TIMEZONE_OFFSET_SECONDS

DateTime = namedtuple(
    "DateTime",
    ("year", "month", "day", "hour", "minute", "second", "weekday", "yearday"),
)


def get_timestamp(tz_offset=TIMEZONE_OFFSET_SECONDS):
    return time() + tz_offset


def get_localtime(tz_offset=TIMEZONE_OFFSET_SECONDS):
    return localtime(get_timestamp(tz_offset=tz_offset))


def iso_time_string_from_timestamp(timestamp):
    time_structure = DateTime(*localtime(timestamp))
    return "{year}-{month}-{day}T{hour}:{minute}:{second}Z".format(
        year=time_structure.year,
        month=time_structure.month,
        day=time_structure.day,
        hour=time_structure.hour,
        minute=time_structure.minute,
        second=time_structure.second,
    )
