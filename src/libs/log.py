import gc
import ujson
from time import time

from libs.data_sender import DataSender
from libs.dt import iso_time_string_from_timestamp
from settings import BOARD_FINGERPRINT, BOARD_HASH_SALT, LOGS_SEND_TO_SERVER, LOGS_SAVE_TO_FILE, LOGS_SERVER_URL


class _Log:
    def __init__(self, log_file, log_url=None):
        self._log_file = log_file
        self._log_sender = None
        if log_url:
            self._log_sender = DataSender(url=log_url, fingerprint=BOARD_FINGERPRINT, hash_salt=BOARD_HASH_SALT)

    def write(self, *args, send_to_server=LOGS_SEND_TO_SERVER, write_to_file=LOGS_SAVE_TO_FILE):
        gc.collect()
        print(*args)
        if self._log_sender and send_to_server:
            try:
                self._log_sender.send_data(data=ujson.dumps({"message": " ".join([str(i) for i in args])}))
            except Exception as exc:
                print("[LOG ERROR] Can not send data, enabling saving ", exc)
                write_to_file = True
        if write_to_file:
            try:
                with open(self._log_file, "a") as fp:
                    fp.write("[{}] ".format(iso_time_string_from_timestamp(timestamp=time())))
                    fp.write(" ".join([str(i) for i in args]))
                    fp.write("\n")
            except Exception as exc:
                print("[LOG ERROR] Can not save data to file {}, {}".format(self._log_file, exc))
        gc.collect()


LOGGER = _Log(
    log_file="messages.log",
    log_url=LOGS_SERVER_URL,
)
