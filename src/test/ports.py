from machine import ADC, Pin
from time import sleep


def test_ads(pin_number):
    pin = ADC(Pin(pin_number), atten=ADC.ATTN_11DB)
    for i in range(10):
        print("Pin {}, value {}".format(pin_number, pin.read_u16()))
        sleep(0.5)


def test_gpio_out(pin_number):
    pin = Pin(pin_number, Pin.OUT)
    set_state = True
    for i in range(10):
        pin.value(set_state)
        print("GPIO Pin {} set to {}".format(pin_number, set_state))
        set_state = not set_state
        sleep(0.5)


def run():
    test_gpio_out(pin_number=4)
    test_ads(pin_number=32)
    test_ads(pin_number=33)
    test_gpio_out(pin_number=27)
    test_gpio_out(pin_number=15)
