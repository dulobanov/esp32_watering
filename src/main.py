from machine import ADC, Pin, lightsleep, deepsleep
from time import time, sleep

from libs.dt import get_localtime
from libs.soil_moisture_sensor import SoilMoistureSensor
from libs.telemetry_sender import telemetry_sender, TelemetrySample
from settings import (
    WATERING_PIN, SIGNAL_PIN, SOIL_HUMIDITY_PIN_0, SOIL_TRIGGER_HUMIDITY, WATER_VOL, ML_PER_SEC, SOIL_WATERING_TIMEOUT,
)

from libs.log import LOGGER
from state import STATE

SOIL_MOISTURE_MEASUREMENTS_TIMEOUT = 5 * 60  # 5 minutes


def watering():
    signal_pin = Pin(SIGNAL_PIN, Pin.OUT)
    watering_pin = Pin(WATERING_PIN, Pin.OUT)
    soil_humidity_sensor = SoilMoistureSensor(SOIL_HUMIDITY_PIN_0, dry_value=39000, aqua_value=9000)

    # while True:
    #     print("Soil humidity values", soil_humidity_sensor.get_raw_value(), soil_humidity_sensor.get_moisture())
    #     watering_required = soil_humidity_sensor.get_moisture() < SOIL_TRIGGER_HUMIDITY
    #     sleep(1)

    while True:
        moisture = soil_humidity_sensor.get_moisture()
        LOGGER.write(
            "Soil humidity values", soil_humidity_sensor.get_raw_value(), moisture,
            send_to_server=False,
            write_to_file=False,
        )
        telemetry_sender.send_measurements(samples=(
            TelemetrySample(
                timestamp=time(),
                measurement="soil_moisture_sensor_0",
                value=moisture,
            ),
        ))

        watering_required = moisture < SOIL_TRIGGER_HUMIDITY
        LOGGER.write("Minimal humidity is {}, current is {}, watering required {}".format(
            SOIL_TRIGGER_HUMIDITY, moisture, watering_required,
        ))

        if watering_required:
            watering_pin.on()
            sleep_ec = round(WATER_VOL / ML_PER_SEC)
            LOGGER.write("Start watering {} for {}".format(get_localtime(), sleep_ec))
            end_time = time() + sleep_ec

            LOGGER.write("Current time is {}, end time is {}".format(time(), end_time))
            while time() < end_time:
                signal_pin.on()
                sleep(0.5)
                signal_pin.off()
                sleep(0.5)
                STATE.alive()

            watering_pin.off()
            LOGGER.write("Watering finished {}".format(get_localtime()))
            telemetry_sender.send_measurements(samples=(
                TelemetrySample(
                    timestamp=time(),
                    measurement="watering",
                    value=WATER_VOL,
                ),
            ))
        else:
            LOGGER.write("Soil moisture is enough, watering skipped")

        # waiting for next watering
        end_time = time() + SOIL_WATERING_TIMEOUT
        next_moisture_measurement = time() + SOIL_MOISTURE_MEASUREMENTS_TIMEOUT
        while time() < end_time:
            if time() >= next_moisture_measurement:
                LOGGER.write(
                    "Next watering in {} seconds".format(end_time - time()),
                    send_to_server=False,
                    write_to_file=False,
                )
                moisture = soil_humidity_sensor.get_moisture()
                LOGGER.write(
                    "Current moisture is {}".format(moisture),
                    send_to_server=False,
                    write_to_file=False,
                )
                telemetry_sender.send_measurements(samples=(
                    TelemetrySample(
                        timestamp=time(),
                        measurement="soil_moisture_sensor_0",
                        value=moisture,
                    ),
                ))
                next_moisture_measurement = time() + SOIL_MOISTURE_MEASUREMENTS_TIMEOUT
            signal_pin.on()
            sleep(0.1)
            signal_pin.off()
            sleep(9.9)
            STATE.alive()


if __name__ == "__main__":
    watering()
