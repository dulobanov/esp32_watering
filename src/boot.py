import machine
from time import localtime

from machine import reset, reset_cause

from libs.log import LOGGER
from libs.dt import get_localtime
from state import STATE

RESET_CAUSE_MAPPING = {
    machine.PWRON_RESET: "PWRON_RESET",
    machine.HARD_RESET: "HARD_RESET",
    machine.WDT_RESET: "WDT_RESET",
    machine.DEEPSLEEP_RESET: "DEEPSLEEP_RESET",
    machine.SOFT_RESET: "SOFT_RESET",
}


def get_reset_cause():
    value = reset_cause()
    return "RESET cause code is '{}', '{}'".format(value, RESET_CAUSE_MAPPING.get(value, "UNKNOWN"))


if __name__ == "__main__":
    STATE.reset_watering()
    LOGGER.write(
        ">>> Starting, connecting to WiFi <<<, {} ".format(get_reset_cause()),
        send_to_server=False,
        write_to_file=True,
    )

    STATE.connect_to_wifi(
        essid="Keenetic-6579",
        password="6zvTJw34",
    )

    LOGGER.write(">>> Started <<<, {} ".format(get_reset_cause()))

    LOGGER.write("Current UTC time is {}.".format(localtime()), send_to_server=False, write_to_file=True)
    LOGGER.write("Current localtime time is {}.".format(get_localtime()), send_to_server=False, write_to_file=True)
