import sys

from unittest import TestCase
from mock import MagicMock

machine_mock = MagicMock()
sys.modules["machine"] = machine_mock

from libs.soil_moisture_sensor import SoilMoistureSensor


class TestSoilMoistureSensor(TestCase):
    def test_positive_grow_mid(self):
        machine_mock.ADC.return_value.read_u16.return_value = 60
        sensor = SoilMoistureSensor(1, 20, 100)
        self.assertEqual(sensor.get_moisture(), 50)

    def test_positive_grow_below_min(self):
        machine_mock.ADC.return_value.read_u16.return_value = 10
        sensor = SoilMoistureSensor(1, 20, 100)
        self.assertEqual(sensor.get_moisture(), 0)

    def test_positive_grow_above_max(self):
        machine_mock.ADC.return_value.read_u16.return_value = 110
        sensor = SoilMoistureSensor(1, 20, 100)
        self.assertEqual(sensor.get_moisture(), 100)

    def test_positive_grow(self):
        machine_mock.ADC.return_value.read_u16.return_value = 30
        sensor = SoilMoistureSensor(1, 20, 100)
        self.assertEqual(sensor.get_moisture(), 12.5)

    def test_negative_grow(self):
        machine_mock.ADC.return_value.read_u16.return_value = 120
        sensor = SoilMoistureSensor(1, 200, 100)
        self.assertEqual(sensor.get_moisture(), 80)

    def test_descriptor(self):
        machine_mock.ADC.return_value.read_u16.return_value = 120

        class TestClass:
            sensor = SoilMoistureSensor(1, 500, 100)

            def get(self):
                return self.sensor

        test_instance = TestClass()
        self.assertEqual(test_instance.get(), 95)
